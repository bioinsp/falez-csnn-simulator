#ifndef _PROCESS_H
#define _PROCESS_H

#include <iostream>

#include "Input.h"
#include "Color.h"
#include "ClassParameter.h"



class Process : public ClassParameter {

	friend class AbstractExperiment;

public:
	template<typename T, typename Factory>
	Process(const RegisterClassParameter<T, Factory>& registration) :
		ClassParameter(registration), _output_shape() {

	}

	virtual Shape resize(const Shape& shape) = 0;
	virtual size_t train_pass_number() const = 0;
	virtual void process_train_sample(const std::string& label, Tensor<float>& sample, size_t current_pass) = 0;
	virtual void process_test_sample(const std::string& label, Tensor<float>& sample) = 0;
	const Shape& shape() const {
		return _output_shape;
	}

private:
	void _resize(const Shape& shape) {
		_output_shape = resize(shape);
	}

	Shape _output_shape;

};

class UniquePassProcess : public Process {

public:
	template<typename T, typename Factory>
	UniquePassProcess(const RegisterClassParameter<T, Factory>& registration) : Process(registration) {

	}

	virtual size_t train_pass_number() const {
		return 1;
	}

	virtual void process_train_sample(const std::string& label, Tensor<float>& sample, size_t) {
		process_train(label, sample);
	}

	virtual void process_test_sample(const std::string& label, Tensor<float>& sample) {
		process_test(label, sample);
	}

	virtual void process_train(const std::string& label, Tensor<float>& sample) = 0;
	virtual void process_test(const std::string& label, Tensor<float>& sample) = 0;
};

class TwoPassProcess : public Process {

public:
	template<typename T, typename Factory>
	TwoPassProcess(const RegisterClassParameter<T, Factory>& registration) : Process(registration) {

	}

	virtual size_t train_pass_number() const {
		return 2;
	}

	virtual void process_train_sample(const std::string& label, Tensor<float>& sample, size_t current_pass) {
		if(current_pass == 0) {
			compute(label, sample);
		}
		else {
			process_train(label, sample);
		}
	}

	virtual void process_test_sample(const std::string& label, Tensor<float>& sample) {
		process_test(label, sample);
	}

	virtual void compute(const std::string& label, const Tensor<float>& sample) = 0;
	virtual void process_train(const std::string& label, Tensor<float>& sample) = 0;
	virtual void process_test(const std::string& label, Tensor<float>& sample) = 0;
};



class ProcessFactory : public ClassParameterFactory<Process, ProcessFactory> {


public:
	ProcessFactory() : ClassParameterFactory<Process, ProcessFactory>("Process") {

	}

};


#endif
